precision mediump float;

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;
attribute vec3 position;

void main() {
    mat4 modelViewMatrix = viewMatrix * modelMatrix;
    //mat4 modelViewMatrix = modelMatrix * viewMatrix;
    vec4 mvPosition = modelViewMatrix * vec4(position, 1.0);
    gl_Position = projectionMatrix * mvPosition;
}