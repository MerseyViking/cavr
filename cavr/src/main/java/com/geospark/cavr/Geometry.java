package com.geospark.cavr;

import android.opengl.GLES20;
import android.opengl.Matrix;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

public class Geometry {
    private float[] _xform = new float[16];
    private FloatBuffer _vertices;
    private ShortBuffer _faces;

    public Geometry() {
        ByteBuffer bb = ByteBuffer.allocateDirect(8 * 3 * 4);
        bb.order(ByteOrder.nativeOrder());

        _vertices = bb.asFloatBuffer();
        float verts[] = {
                5.0f, -5.0f, -5.0f,
                5.0f, -5.0f, 5.0f,
                -5.0f, -5.0f, 5.0f,
                -5.0f, -5.0f, -5.0f,
                5.0f, 5.0f, -5.0f,
                5.0f, 5.0f, 5.0f,
                -5.0f, 5.0f, 5.0f,
                -5.0f, 5.0f, -5.0f
        };

        _vertices.put(verts);
        _vertices.position(0);

        ByteBuffer fbb = ByteBuffer.allocateDirect(12 * 3 * 2);
        fbb.order(ByteOrder.nativeOrder());

        _faces = fbb.asShortBuffer();
        short faces[] = {
                0, 1, 2,
                0, 2, 3,
                4, 7, 6,
                4, 6, 5,
                0, 4, 5,
                0, 5, 1,
                1, 5, 6,
                1, 6, 2,
                2, 6, 7,
                2, 7, 3,
                4, 0, 3,
                4, 3, 7
        };

        _faces.put(faces);
        _faces.position(0);

        Matrix.setIdentityM(_xform, 0);
    }

    public void rotate(float angle) {
        Matrix.rotateM(_xform, 0, angle, 0.0f, 0.0f, 1.0f);
        Matrix.translateM(_xform, 0, 0.0f, 200.0f, 0.0f);
    }

    public void draw(int shader) {
        int modelMatrixHandle = GLES20.glGetUniformLocation(shader, "modelMatrix");
        GLES20.glUniformMatrix4fv(modelMatrixHandle, 1, false, _xform, 0);

        int positionHandle = GLES20.glGetAttribLocation(shader, "position");
        GLES20.glEnableVertexAttribArray(positionHandle);
        GLES20.glVertexAttribPointer(positionHandle, 3, GLES20.GL_FLOAT, false, 0, _vertices);
        GLES20.glDrawElements(GLES20.GL_TRIANGLES, 12 * 3, GLES20.GL_UNSIGNED_SHORT, _faces);

        GLES20.glDisableVertexAttribArray(positionHandle);
    }
}
