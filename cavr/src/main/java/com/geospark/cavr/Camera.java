package com.geospark.cavr;

import android.content.Context;
import android.graphics.Point;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.opengl.Matrix;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;

public class Camera implements SensorEventListener {
    private static final float INCHES_IN_A_METRE = 39.3700787f;
    private float[] _viewMatrix = new float[16];
    private float[] _projectionMatrix = new float[16];
    private float _pixelDensityX;
    private float _pixelDensityY;
    private float _aspectRatio;
    // These lengths are in metres.
    private float _screenWidth;
    private float _screenHeight;
    private float _distanceFromEye;
    private final SensorManager _sensor_manager;
    private float[] _gravity = new float[3];
    private float[] _geomagnetic = new float[3];

    private static final float ALPHA = 0.2f;

    public Camera(Context ctx) {
        DisplayMetrics metrics = ctx.getResources().getDisplayMetrics();

        _pixelDensityX = metrics.xdpi;
        _pixelDensityY = metrics.ydpi;

        // A reasonable default.
        _distanceFromEye = 0.5f;

        Matrix.setLookAtM(_viewMatrix, 0, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);

        _sensor_manager = (SensorManager)ctx.getSystemService(Context.SENSOR_SERVICE);
        _sensor_manager.registerListener(this, _sensor_manager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_NORMAL);
        _sensor_manager.registerListener(this, _sensor_manager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD), SensorManager.SENSOR_DELAY_NORMAL);
    }

    public void viewportChanged(int width, int height) {
        // On my Note, these figures are about 9% too small.
        _screenWidth = (width / _pixelDensityX) / INCHES_IN_A_METRE;
        _screenHeight = (height / _pixelDensityY) / INCHES_IN_A_METRE;
        _aspectRatio = (float)width / height;

        Matrix.frustumM(_projectionMatrix, 0, -_screenWidth / 2.0f, _screenWidth / 2.0f, -_screenHeight / 2.0f, _screenHeight / 2.0f, _distanceFromEye, 10000.0f);
    }

    public float[] getViewMatrix() {
        return _viewMatrix;
    }

    public float[] getProjectionMatrix() {
        return _projectionMatrix;
    }

    /**
     *
     * @return the horizontal field of view in radians.
     */
    public float getHFoV() {
        return (float)Math.atan((_screenWidth / 2.0f) / _distanceFromEye) * 2.0f;
    }

    /**
     *
     * @return the vertical field of view in radians.
     */
    public float getVFoV() {
        return (float)Math.atan((_screenHeight / 2.0f) / _distanceFromEye) * 2.0f;
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.accuracy == SensorManager.SENSOR_STATUS_UNRELIABLE) {
            return;
        }

        // Gets the value of the sensor that has been changed
        switch (event.sensor.getType()) {
            case Sensor.TYPE_ACCELEROMETER:
                lowPass(_gravity, event.values);
                break;
            case Sensor.TYPE_MAGNETIC_FIELD:
                lowPass(_geomagnetic, event.values);
                break;
        }

        SensorManager.getRotationMatrix(_viewMatrix, null, _gravity, _geomagnetic);
        // If the screen is in landscape mode, we will need to call remapCoordinateSystem() with, I think, these parameters.
//        float[] orientation = new float[16];
//        SensorManager.getRotationMatrix(orientation, null, _gravity, _geomagnetic);
//        SensorManager.remapCoordinateSystem(orientation, SensorManager.AXIS_Y, SensorManager.AXIS_MINUS_X, _viewMatrix);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    public void lowPass(float[] output, float[] input) {
        output[0] = (ALPHA * output[0]) + ((1.0f - ALPHA) * input[0]);
        output[1] = (ALPHA * output[1]) + ((1.0f - ALPHA) * input[1]);
        output[2] = (ALPHA * output[2]) + ((1.0f - ALPHA) * input[2]);
    }
}
