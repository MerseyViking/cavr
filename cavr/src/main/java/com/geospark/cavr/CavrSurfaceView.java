package com.geospark.cavr;

import android.content.Context;
import android.content.res.TypedArray;
import android.opengl.GLSurfaceView;
import android.util.AttributeSet;

public class CavrSurfaceView extends GLSurfaceView {
    public CavrSurfaceView(Context context) {
        super(context);

        init(context);
    }

    public CavrSurfaceView(Context context, AttributeSet attrs) {
        super(context, attrs);

        if (!isInEditMode()) {
            init(context);
        }
    }

    private void init(Context context) {
        Camera camera = new Camera(context);

        setEGLContextClientVersion(2);
        setRenderer(new CavrRenderer(camera, context));
        setRenderMode(GLSurfaceView.RENDERMODE_CONTINUOUSLY);
    }
}
