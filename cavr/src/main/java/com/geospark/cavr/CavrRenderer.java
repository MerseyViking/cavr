package com.geospark.cavr;

import android.content.Context;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.opengl.Matrix;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

/**
 * Uses a right-handed coordinate system, with +X == east, +Y == north, +Z == up
 */
public class CavrRenderer implements GLSurfaceView.Renderer {
    private int _shader = -1;
    private int _viewMatrixHandle = -1;
    private int _projectionMatrixHandle = -1;
    private Camera _camera = null;

    private Geometry[] _geom = null;
    private String _vert_shader = null;
    private String _frag_shader = null;

    private static final int num_cubes = 16;

    public CavrRenderer(Camera camera, Context context) {
        _camera = camera;
        _geom = new Geometry[num_cubes];
        for (int i = 0; i < num_cubes; i++) {
            _geom[i] = new Geometry();
            _geom[i].rotate(((float)i / num_cubes) * 360.0f);
        }

        try {
            InputStream stream = context.getAssets().open("vertex.glsl");

            int size = stream.available();
            byte[] buffer = new byte[size];
            stream.read(buffer);
            stream.close();
            _vert_shader = new String(buffer);
        } catch (IOException e) {
            // Handle exceptions here
        }

        try {
            InputStream stream = context.getAssets().open("frag.glsl");

            int size = stream.available();
            byte[] buffer = new byte[size];
            stream.read(buffer);
            stream.close();
            _frag_shader = new String(buffer);
        } catch (IOException e) {
            // Handle exceptions here
        }
    }

    @Override
    public void onSurfaceCreated(GL10 unused, EGLConfig config) {
        GLES20.glClearColor(0.1f, 0.3f, 0.05f, 1.0f);
        loadShaders();
    }

    @Override
    public void onSurfaceChanged(GL10 unused, int width, int height) {
        GLES20.glViewport(0, 0, width, height);
        _camera.viewportChanged(width, height);
    }

    @Override
    public void onDrawFrame(GL10 unused) {
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);
        GLES20.glUseProgram(_shader);
        CavrRenderer.checkGlError("glUseProgram");
        _viewMatrixHandle = GLES20.glGetUniformLocation(_shader, "viewMatrix");
        _projectionMatrixHandle = GLES20.glGetUniformLocation(_shader, "projectionMatrix");
        GLES20.glUniformMatrix4fv(_viewMatrixHandle, 1, false, _camera.getViewMatrix(), 0);
        GLES20.glUniformMatrix4fv(_projectionMatrixHandle, 1, false, _camera.getProjectionMatrix(), 0);

        for (int i = 0;i < num_cubes; i++) {
            _geom[i].draw(_shader);
        }
    }

    public void loadShaders() {
        int vertex_shader = GLES20.glCreateShader(GLES20.GL_VERTEX_SHADER);
        GLES20.glShaderSource(vertex_shader, _vert_shader);
        GLES20.glCompileShader(vertex_shader);
        CavrRenderer.checkGlError("glCompileShader - vertex");
        int fragment_shader = GLES20.glCreateShader(GLES20.GL_FRAGMENT_SHADER);
        GLES20.glShaderSource(fragment_shader, _frag_shader);
        GLES20.glCompileShader(fragment_shader);
        CavrRenderer.checkGlError("glCompileShader - fragment");

        _shader = GLES20.glCreateProgram();
        GLES20.glAttachShader(_shader, vertex_shader);
        GLES20.glAttachShader(_shader, fragment_shader);
        GLES20.glLinkProgram(_shader);

        CavrRenderer.checkGlError("glLinkProgram");
    }

    public int getShader() {
        return _shader;
    }

    public static void checkGlError(String glOperation) {
        int error;
        while ((error = GLES20.glGetError()) != GLES20.GL_NO_ERROR) {
            Log.e("Cavr", glOperation + ": glError " + error);
            throw new RuntimeException(glOperation + ": glError " + error);
        }
    }
}
